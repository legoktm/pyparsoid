/*
Copyright (C) 2021 Kunal Mehta <legoktm@member.fsf.org>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
use parsoid::prelude::*;
use pyo3::prelude::*;

#[pymodule]
fn pyparsoid(_py: Python, m: &PyModule) -> PyResult<()> {
    m.add_class::<PyWikicode>()?;
    Ok(())
}

#[pyclass(unsendable)]
struct PyWikicode {
    code: Wikicode,
}

impl From<Wikicode> for PyWikicode {
    fn from(code: Wikicode) -> Self {
        PyWikicode { code }
    }
}

#[pymethods]
impl PyWikicode {
    #[staticmethod]
    fn new(body: &str) -> Self {
        Wikicode::new(body).into()
    }

    #[staticmethod]
    fn new_node(tag: &str) -> Self {
        Wikicode::new_node(tag).into()
    }

    #[staticmethod]
    fn new_text(text: &str) -> Self {
        Wikicode::new_text(text).into()
    }

    fn revision_id(&self) -> Option<u32> {
        self.code.revision_id()
    }
}
